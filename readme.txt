1. Vorbereitung

Kenntnisse im Umgang mit SCP und SSH werden vorausgesetzt. Unter Windows kannst Du WinSCP und PuTTY nutzen. Der UniFi-AP hat standardmäßig die IP-Adresse 192.168.1.20, die UniFi-Controller-Software wird nicht benötigt. Standarduser und Passwort sind ubnt/ubnt. Du benötigst außerdem eine alte Firmware von Ubiquiti in Version 3.4.7 (z.B. qca956x.v3.4.7.3284.150911.1650.bin).

Die aktuelle Firmware ab Version 3.4.14 (seit Anfang 2016) lässt beim Upgrade nur noch signierte Images zu, so dass es unmöglich ist, die Freifunk-Firmware zu flashen; der AP startet stets mit seiner Originalfirmware.

Diese Anleitung wurde mit UniFi® AP-AC-LITE und UAP-AC-M getestet.

2. Downgrade

    Kopiere die fwupdate.bin per SCP ins Verzeichnis /tmp/ des AP.
    Gib folgendes Kommando per SSH ein:
    syswrapper.sh upgrade2 &
    Es dauert eine Weile, dann bootet der AP neu.

3. Freifunk-Image aufspielen

    Lade nun die aktuelle Freifunk-Firmware für UNIFI-AC-LITE (bzw. UNIFI-AC-MESH) herunter. Es handelt sich stets um ein sysupgrade, versuche also gar nicht, das Image im factory-Verzeichnis zu finden!
    Kopiere sie per SCP ins Verzeichnis /tmp/ des AP.
    Gib folgende Kommandos per SSH ein (Dateinamen anpassen):
    > mtd write /tmp/dein_image-ubiquiti-unifi-ac-lite-sysupgrade.bin kernel0
    Partition 0 wird geschrieben, das dauert wieder etwas.
    Danach muss mit diesem Kommando die zweite Bootpartition gelöscht werden:
    > mtd erase kernel1
    Um sicherzustellen, dass der AP künftig von kernel0 startet, muss das bootselector-flag entsprechend gesetzt werden. Dazu prüfst Du sicherheitshalber, ob mtd4 tatsächlich die Partition “bs” ist:
    BZ.v3.4.7# cat /proc/mtd
    dev: size erasesize name
    mtd0: 00060000 00010000 "u-boot"
    mtd1: 00010000 00010000 "u-boot-env"
    mtd2: 00790000 00010000 "kernel0"
    mtd3: 00790000 00010000 "kernel1"
    mtd4: 00020000 00010000 "bs"
    mtd5: 00040000 00010000 "cfg"
    mtd6: 00010000 00010000 "EEPROM"
    Wenn ja, gib dieses Kommando ein:
    > dd if=/dev/zero bs=1 count=1 of=/dev/mtd4
    Wenn eine andere Partition angezeigt wurde (beispielsweise mtd7: “bs”), muss der Befehl geändert werden (im diesem Falle):
    > dd if=/dev/zero bs=1 count=1 of=/dev/mtd7
    Danach bootes Du deinen AP neu:
    reboot

4. Konfiguration

Logge Dich nun mit dem Browser in 192.168.1.1 ein und konfiguriere Deinen neuen Freifunk-Knoten.
Viel Erfolg und viel Spaß mit Deinem UniFi AC Lite AP!

