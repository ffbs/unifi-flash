#!/bin/bash
set -e
echo "This script automatically flashes Unifi routers with Gluon."
echo "Make sure to have a static ip in 192.168.1.0/24 configured on your ethernet device."

if [ -z $1 ]; then
    echo "MISSING ARG: gluon firmware"
    exit 1
fi
FWFILE=$1
echo -e "Starting, good luck!\n"

SSHOPTS="-o StrictHostKeyChecking=no -o PreferredAuthentications=password -o PubkeyAuthentication=no -o UserKnownHostsFile=/tmp/ubntflash.hosts"
TARGET="192.168.1.20"
export SSHPASS="ubnt"
SCP="sshpass -e scp ${SSHOPTS}"
SSH="sshpass -e ssh ${SSHOPTS} ubnt@${TARGET}"

rm -f /tmp/ubntflash.hosts

function ping_wait {
    until ping -c1 "$1" >/dev/null 2>&1; do :; done &
    trap "kill $!" SIGINT
    wait $!
    trap - SIGINT
}

echo "waiting for router to appear"
ping_wait $TARGET

echo "Step 1: flashing older firmware"
$SCP 'fwupdate.bin' "ubnt@${TARGET}:/tmp/"
$SSH 'sh -c "syswrapper.sh upgrade2 & sleep 2"' || true

echo "waiting for router to come back"
sleep 5
ping_wait $TARGET

echo "Step 2: flashing Gluon"
$SCP $FWFILE "ubnt@${TARGET}:/tmp/"
$SSH "mtd write /tmp/$1 kernel0"
$SSH 'mtd erase kernel1'
bs_part=$($SSH 'cat /proc/mtd' | grep -F '"bs"' | cut -d':' -f1)
$SSH "dd if=/dev/zero bs=1 count=1 of=/dev/$bs_part"
$SSH 'reboot'
rm -f /tmp/ubntflash.hosts

echo "Rebooting, Waiting for router to enter config mode."
sleep 5
ping_wait 192.168.1.1
echo "DONE, open http://192.168.1.1"

